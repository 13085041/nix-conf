{

  imports = [
    ./zsh.nix
    ./modules/bundle.nix
  ];

  home = {
    username = "redf";
    homeDirectory = "/home/redf";
    stateVersion = "23.11";
  };
}