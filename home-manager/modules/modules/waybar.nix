{
    programs.waybar = {
        enable = true;
        settings = {
            mainBar = {
                height = "30";
                width = "1000";
                spacing = 0;
                position = "bottom";
                margin-bottom = "3";

                modules-left = ["cpu" "custom/mem" "temperature" "battery"];
                modules-center = ["hyprland/workspaces"];
                modules-right = ["tray" "backlight" "pulseaudio" "hyprland/language" "clock"];

                "hyprland/workspaces" = {
                disable-scroll = true;
                };

            "hyprland/language" = {
                format-en = "US";
                format-ru = "RU";
                min-length = 5;
                tooltip = false;
            };

            "clock" = {
                # timezone = "America/New_York";
                tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
                format = "{:%Y-%m-%d}";
            };

            "pulseaudio" = {
                # scroll-step = 1; # %, can be a float
                reverse-scrolling = 1;
                format = "{volume}% {icon} {format_source}";
                format-bluetooth = "{volume}% {icon} {format_source}";
                format-bluetooth-muted = " {icon} {format_source}";
                format-muted = " {format_source}";
                format-source = "{volume}% ";
                format-source-muted = "";
                format-icons = {
                    headphone = "";
                    hands-free = "";
                    headset = "";
                    phone = "";
                    portable = "";
                    car = "";
                    default = ["" "" ""];
                };
                on-click = "pavucontrol";
                #min-length = 13;
            };

            "custom/mem" = {
                format = "{} ";
                interval = 3;
                exec = "free -h | awk '/Mem:/{printf $3}'";
                tooltip = false;
            };

            "cpu" = {
                interval = 2;
                format = "{usage}% ";
                min-length = 6;
            };

            "temperature" = {
                # thermal-zone = 2;
                # hwmon-path = "/sys/class/hwmon/hwmon2/temp1_input";
                critical-threshold = 80;
                # format-critical = "{temperatureC}°C {icon}";
                format = "{temperatureC}°C {icon}";
                format-icons = ["" "" "" "" ""];
                tooltip = false;
            };

            "backlight" = {
                device = "intel_backlight";
                format = "{percent}% {icon}";
                format-icons = [""];
                min-length = 7;
            };

            battery = {
                states = {
                    warning = 30;
                    critical = 15;
                };
                format = "{capacity}% {icon}";
                format-charging = "{capacity}% ";
                format-plugged = "{capacity}% ";
                format-alt = "{time} {icon}";
                format-icons = ["" "" "" "" "" "" "" "" "" ""];
                on-update = "$HOME/.config/waybar/scripts/check_battery.sh";
            };

            tray = {
                icon-size = 16;
                spacing = 0;
            };

                };
            };

            style = 
            ''
        * {
            /* `otf-font-awesome` is required to be installed for icons */
            font-family: JetBrains Mono;
            font-weight: bold;
            font-size: 18px
            min-height: 0px;
        }

        @define-color bg #141414;
        @define-color fg #f9f9f9;
        @define-color zad #0A0A0A;
        @define-color main #C83F49;
        @define-color gr #323232;

        window#waybar {
            background-color: @bg;
            transition-property: background-color;
            transition-duration: .5s;
            border-radius: 5;
        }

        window#waybar.hidden {
            opacity: 0.2;
        }

        #workspaces {
            background-color: @zad;
            margin: 4px 5px 4px 5px;
            border-radius: 6;
        }

        #workspaces button {
            background-color: @zad;
            color: @fg;	
            padding: 0 4px;
            margin: 2px 5px 0 5px;
            border-bottom: solid 2px #23252e;
            border-radius: 0;
        }

        #workspaces button:hover {
            background-color: @zad;
            color: @fg;	
            padding: 0 4px;
            margin: 2px 5px 0 5px;
            border-bottom: solid 2px @fg;
            border-radius: 0;
        }

        #workspaces button.active {
            background-color: @zad;
            color: @main;
            padding: 0 4px;
            margin: 2px 5px 0 5px;
            border-bottom: solid 2px;
            border-radius: 0;
        }

        #workspaces button.empty {
            color: @gr;
            padding: 0 4px;
            margin: 2px 5px 0 5px;
            border-bottom: solid 2px #23252e;
            border-radius: 0;
        }

        #language {
            background: @zad;
            color: @main;
            padding: 0;
            margin: 4px 0 4px 0;
            border-bottom: solid 2px @main;
            border-radius: 0;
        }

        #clock {
            background: @zad;
            color: @fg;
            padding: 0 15px 0 29px;
            margin: 4px 10px 4px 0;
            border-radius: 0 6 6 0;
            font-size: 16px;
        }

        #pulseaudio {
            margin-right: 8px;
            padding-left: 16px;
            padding-right: 16px;
            border-radius: 10px;
            transition: none;
            color: #ffffff;
            background: #383c4a;
        }

        #pulseaudio.muted {
            background-color: #90b1b1;
            color: #2a5c45;
        }

        #custom-mem {
            margin-right: 8px;
            padding-left: 16px;
            padding-right: 16px;
            border-radius: 10px;
            transition: none;
            color: #ffffff;
            background: #383c4a;
        }

        #cpu {
            background: @zad;
            color: @fg;
            padding: 0 30 0 15px;
            margin: 4px 0px 4px 0;
        }

        #temperature {
        background: @zad;
            color: @fg;
            padding: 0 30 0 15px;
            margin: 4px 0px 4px 0; 
        }

        #temperature.critical {
            background-color: #eb4d4b;
        }

        #backlight {
            background: @zad;
            color: @fg;
            padding: 0 30 0 15px;
            margin: 4px 0px 4px 0;
        }

        #battery {
        background: @zad;
            color: @fg;
            padding: 0 30 0 15px;
            margin: 4px 0px 4px 0; 
        }

        #battery.charging {
            color: #ffffff;
            background-color: #26A65B;
        }

        #battery.warning:not(.charging) {
            background-color: #ffbe61;
            color: black;
        }

        #battery.critical:not(.charging) {
            background-color: #f53c3c;
            color: #ffffff;
            animation-name: blink;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-direction: alternate;
        }

        #tray {
            background: @zad;
            color: @fg;
            padding: 0 15px 0 15px;
            margin: 4px 0 4px 0;
            border-radius: 6 0 0 6;
        }

        @keyframes blink {
            to {
                background-color: #ffffff;
                color: #000000;
            }
        }
                '';
            };
        }