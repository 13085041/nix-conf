{
  imports = [
    ./cursor.nix
    ./git.nix
    ./kitty.nix
    ./helix.nix
    ./hyprland.nix
    ./waybar.nix
  ];
}