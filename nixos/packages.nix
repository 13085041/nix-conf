{ pkgs, ... }: {
  nixpkgs.config = {
    allowUnfree = true;
  };

  environment.systemPackages = with pkgs; [
    # Desktop apps
    librewolf
    telegram-desktop
    foot
    wofi
    mpv
    kdenlive
    krita
    gparted
    logseq
    nemo
    qbittorrent
    tor-browser-bundle-bin
    keepassxc
    libreoffice-fresh
    vscodium
    bleachbit
    wine

    # Coding stuff
    gnumake
    gcc
    python
    # (python3.withPackages (ps: with ps; [ requests ]))

    # CLI utils
    file
    tree
    wget
    git
    fastfetch
    htop
    nix-index
    unzip
    scrot
    ffmpeg
    light
    lux
    mediainfo
    zram-generator
    dbus-broker
    zip
    mc
    ntfs3g
    yt-dlp
    brightnessctl
    swaybg
    openssl
    lazygit
    bluez
    bluez-tools

    # GUI utils
    swayimg
    screenkey
    mako
    gromit-mpx
    dmenu

    # Xorg stuff
    xterm
    xclip
    xorg.xbacklight

    # Wayland stuff
    xwayland
    wl-clipboard
    cliphist

    # WMs and stuff
    herbstluftwm
    hyprland
    seatd
    xdg-desktop-portal-hyprland
    waybar

    # Sound
    pipewire
    pulseaudio
    pamixer

    # GPU stuff AMD
    amdvlk
    rocm-opencl-icd
    glaxnimate

    # Screenshotting
    grim
    grimblast
    slurp
    flameshot
    swappy

    # Other
    home-manager
  ];

  fonts.packages = with pkgs; [
    jetbrains-mono
    noto-fonts
    noto-fonts-emoji
    twemoji-color-font
    font-awesome
    powerline-fonts
    powerline-symbols
    (nerdfonts.override { fonts = [ "NerdFontsSymbolsOnly" ]; })
  ];
}